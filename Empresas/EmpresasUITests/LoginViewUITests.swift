
//
//  LoginViewTest.swift
//  EmpresasUITests
//
//  Created by Luiz Henrique de Sousa on 18/12/19.
//  Copyright © 2019 Luiz Henrique. All rights reserved.
//

import XCTest

class LoginViewUITests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false
        XCUIApplication().launch()
    }

    func testEmptyFieldsDisabledButton() {
        let app = XCUIApplication()
        let performLoginButton = app.buttons["performLogin"]
        
        XCTAssertFalse(performLoginButton.isEnabled)
    }

    func testEmptyPasswordDisabledButton() {
        let app = XCUIApplication()
        
        let emailField = app.textFields["emailField"]
        let performLoginButton = app.buttons["performLogin"]

        emailField.tap()
        emailField.typeText("testeapple@ioasys.com.br")
        XCTAssertFalse(performLoginButton.isEnabled)
    }
    
    func testEmptyEmailDisabledButton() {
        let app = XCUIApplication()
        
        let passwordField = app.secureTextFields["passwordField"]
        let performLoginButton = app.buttons["performLogin"]

        passwordField.tap()
        passwordField.typeText("12341234")
        XCTAssertFalse(performLoginButton.isEnabled)
    }
    
    func testFilledFormEnabledButton() {
        let app = XCUIApplication()
        
        let emailField = app.textFields["emailField"]
        let passwordField = app.secureTextFields["passwordField"]
        let performLoginButton = app.buttons["performLogin"]

        emailField.tap()
        emailField.typeText("testeapple@ioasys.com.br")
        passwordField.tap()
        passwordField.typeText("12341234")
        XCTAssertTrue(performLoginButton.isEnabled)
    }


}
