//
//  LoginViewModelTest.swift
//  EmpresasTests
//
//  Created by Luiz Henrique de Sousa on 18/12/19.
//  Copyright © 2019 Luiz Henrique. All rights reserved.
//


import XCTest
@testable import Empresas
import RxSwift

class LoginViewModelTests: XCTestCase {

    let viewModel = LoginViewModel()
    let disposeBag = DisposeBag()
    
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testLogin() {
        let testExpectation = expectation(description: "Completed ViewModel Perform Login")
        
        viewModel.email.value = "testeapple@ioasys.com.br"
        viewModel.password.value = "12341234"
        
        viewModel.isSuccess.asObservable()
                  .bind{ value in
                    if(value) {
                        XCTAssertTrue(value)
                        testExpectation.fulfill()
                    }
                        
                  }.disposed(by: disposeBag)
              
        
        viewModel.performLogin()

        
        waitForExpectations(timeout: 10) { error in
          if let error = error {
            XCTFail("waitForExpectations error: \(error) \n Timeout: 5s")
          }
        }
    }
    
    func testLoginWithWrongEmail() {
        let testExpectation = expectation(description: "Completed ViewModel Perform Login Error")
        
        viewModel.email.value = "testeappleioasys"
        viewModel.password.value = "12341234"
        
        viewModel.errorMsg.asObservable()
                  .bind{ value in
                    if(!value.isEmpty) {
                        let customError = CustomError(with: .wrongEmailValidation)
                        XCTAssertEqual(value, customError.description())
                        testExpectation.fulfill()
                    }
                        
                  }.disposed(by: disposeBag)
              
        
        viewModel.performLogin()
        
        waitForExpectations(timeout: 10) { error in
          if let error = error {
            XCTFail("waitForExpectations error: \(error) \n Timeout: 5s")
          }
        }

    }
    
    func testLoginWithWrongPassword() {
        let testExpectation = expectation(description: "Completed ViewModel Perform Password error")
        
        viewModel.email.value = "testeapple@ioasys.com.br"
        viewModel.password.value = "12"
        
        viewModel.errorMsg.asObservable()
                  .bind{ value in
                    if(!value.isEmpty) {
                        let customError = CustomError(with: .wrongPasswordValidation)
                        XCTAssertEqual(value, customError.description())
                        testExpectation.fulfill()
                    }
                        
                  }.disposed(by: disposeBag)
              
        
        
        viewModel.performLogin()
        
        waitForExpectations(timeout: 10) { error in
          if let error = error {
            XCTFail("waitForExpectations error: \(error) \n Timeout: 5s")
          }
        }
    }
    
    /*
    func testLoginWithServerError() {
        let testExpectation = expectation(description: "Completed ViewModel Perform Server Error")
        
        viewModel.email.value = "testeapple@ioasys.com.br"
        viewModel.password.value = "123412344"
        
        viewModel.errorMsg.asObservable()
                  .bind{ value in
                    if(!value.isEmpty) {
                        XCTAssertTrue(true, value)
                        testExpectation.fulfill()
                    }
                        
                  }.disposed(by: disposeBag)
              
        
        
        viewModel.performLogin()
        
        waitForExpectations(timeout: 10) { error in
          if let error = error {
            XCTFail("waitForExpectations error: \(error) \n Timeout: 5s")
          }
        }
    }
    */
    
    func testPerformanceLogin() {
        
        self.measure {
            self.testLogin()
         }
    }


}
