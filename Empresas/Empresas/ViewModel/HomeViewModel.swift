//
//  HomeViewModel.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 09/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import UIKit
import RxSwift

class HomeViewModel {

    private let _session: UserDataManager
    private var service: HomeService
    private var companies: Companies?
    
    var enterpriseList: [Enterprise] = []
    var selectedInformation: Enterprise?
    
    let isSuccess : Variable<Bool> = Variable(false)
    let isLoading : Variable<Bool> = Variable(false)
    let errorMsg  : Variable<String> = Variable("")
    
    var query : Variable<String> = Variable("")
    
    let didSelectedRow = PublishSubject<Void>()
    
    
    var rows: Int {
        return enterpriseList.count
    }
    
    init(session: UserDataManager) {
        _session = session
        service = HomeService()
        requestLogin()
        
    }
    
    func selectedItem(by index: Int) {
        selectedInformation = enterpriseList[index]
        didSelectedRow.onNext(Void())
    }
    
    func getCompanyBy(_ index: Int)->Enterprise? {
        guard let companies = companies else { return nil }
        if(index > companies.enterprises_number) { return nil }
        else { return enterpriseList[index] }
    }
    
    func updateList(with text: String? = nil) {
        guard let companies = companies, let enterprises = companies.enterprises else { return }
        
        if let queryText = text {
            if queryText.isEmpty { return enterpriseList = enterprises}
            else {
                enterpriseList = enterprises.filter({
                    guard let name = $0.enterprise_name else { return false }
                    return name.lowercased().contains(queryText.lowercased())
                    
                })
            }
        } else {
            if(query.value.isEmpty) { return enterpriseList = enterprises }
            else {
                enterpriseList = enterprises.filter({ ($0.enterprise_name?.contains(query.value) ?? false)})
            }
            
        }
    }
    
    func requestLogin() {
        isLoading.value = true
        
        if _session.isLogged {
            service.requestCompanyList() {
                responsePortfolio, responseError in
                if let error = responseError {
                    self.isSuccess.value = false
                    self.isLoading.value = false
                    self.errorMsg.value = error.description()
                }
                if let portfolio = responsePortfolio {
                    // persist info
                    self.companies = portfolio
                    self.updateList()
                    self.isSuccess.value = true
                    self.isLoading.value = false
                    
                }
                else {
                    let parseError = CustomError(with: .parserError)
                    self.isSuccess.value = false
                    self.isLoading.value = false
                    self.errorMsg.value =  parseError.description()
                }
            }
        } else {
            //TODO: Return to sign in
        }
    }

}
