//
//  LoginViewModel.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 18/12/19.
//  Copyright © 2019 Luiz Henrique. All rights reserved.
//

import UIKit
import RxSwift

class LoginViewModel {
    var email            : Variable<String> = .init("")
    var password         : Variable<String> = .init("")
    let isButtonEnabled  : Observable<Bool>
    let disposebag = DisposeBag()
    var currentUser: User?
    
    var service: LoginService
    private let _session: UserDataManager
    
    let isSuccess : Variable<Bool> = Variable(false)
    let isLoading : Variable<Bool> = Variable(false)
    let errorMsg  : Variable<String> = Variable("")
    
    init(session: UserDataManager) {
        service = LoginService()
        _session = session
        
        let isEmailNull = email.asObservable()
        .map { $0.isEmpty }
        .distinctUntilChanged()
        
        let isPasswordNull = password.asObservable()
        .map { $0.isEmpty }
        .distinctUntilChanged()
        
        isButtonEnabled = Observable.combineLatest(isPasswordNull, isEmailNull, isLoading.asObservable()) { (!$0 && !$1) && !$2 }
        
    }
    
    func performLogin() {
        isLoading.value = true
        
        if let credentialsValdiation = validateFields() {
            isSuccess.value = false
            isLoading.value = false
            errorMsg.value = credentialsValdiation.description()
            return
        }
        service.loginPostWith(user: email.value, password: password.value) {
            responseUser, responseError in
            if let error = responseError {
                self.isSuccess.value = false
                self.isLoading.value = false
                self.errorMsg.value = error.description()
            }
            if let user = responseUser, self._session.reloadSession() {
                // persist info
                self.currentUser = user
                self.isSuccess.value = true
                self.isLoading.value = false
                
            }
            else if !self._session.reloadSession() {
                let tokenError = CustomError(with: .unknownError)
                self.isSuccess.value = false
                self.isLoading.value = false
                self.errorMsg.value =  tokenError.description()
            }
            else {
                let parseError = CustomError(with: .parserError)
                self.isSuccess.value = false
                self.isLoading.value = false
                self.errorMsg.value =  parseError.description()
            }
            
        }
        
    }
    
    private func validateFields()-> CustomError? {
        if(!validateEmailPattern(text: email.value)) {
            return CustomError(with: .wrongEmailValidation)
        }
        else if(!validatePasswordPattern(text: password.value)) {
            return CustomError(with: .wrongPasswordValidation)
        }
        return nil
    }
    
    private func validateEmailPattern(text : String) -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: text)
    }
    
    private func validatePasswordPattern(text : String) -> Bool{
        let emailRegEx = "[0-9]{7,64}"
        let emailValidation = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailValidation.evaluate(with: text)
    }
}
