//
//  DetailViewModel.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 10/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import UIKit
import RxSwift

class DetailViewModel {
    var enterpriseInformation: Enterprise?
    
    var enterpriseName: String {
        guard let enterprise = enterpriseInformation, let companyName = enterprise.enterprise_name else { return ""}
        return companyName
    }
    
    var enterpriseDescription: String {
        guard let enterprise = enterpriseInformation, let description = enterprise.description else { return ""}
        return description
    }
    }
