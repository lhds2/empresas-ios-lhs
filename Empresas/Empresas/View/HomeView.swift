//
//  HomeView.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 09/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import UIKit
import MaterialTextField
import RxCocoa
import RxSwift

class HomeView: UIViewController, Storyboarded {
    
    lazy var searchBar = UISearchBar(frame: .zero)
    @IBOutlet weak var tableView: UITableView!
    
    static var storyboard: StoryboardType = .home
    var viewModel : HomeViewModel?
    let disposeBag = DisposeBag()
    
    private var previousColor: UIColor = .white
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController!.isNavigationBarHidden = false
        setupSearch()
        setupTableView()
        setupCallBack()
        
    }
    func setupSearch() {
         searchBar.delegate = self
         searchBar.sizeToFit()
         searchBar.placeholder = "Pesquisar"
         let leftNavBarButton = UIBarButtonItem(customView: searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
         self.navigationItem.leftBarButtonItem?.isEnabled = true
        
        guard let viewModel = self.viewModel else { return }
        
        searchBar.rx.text.orEmpty
        .throttle(.milliseconds(300), scheduler: MainScheduler.instance)
        .distinctUntilChanged()
        .bind(to: viewModel.query)
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
    }
    
    func setupStartSearch() {
        let image = UIImage(named: "search")?.withRenderingMode(.alwaysOriginal)
        let button = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(startSearch(sender:)))
        navigationItem.rightBarButtonItem = button
    }
    
    @objc func startSearch(sender: UIBarButtonItem) {
        setStateBar(with: true)
    }


    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationItem.titleView?.sizeToFit()
    }

    
    func setStateBar(with isSearching: Bool) {
       
        let rightColor: UIColor = isSearching ? .clear : .white
        let leftColor: UIColor = isSearching ? previousColor : .clear
        navigationItem.rightBarButtonItem?.isEnabled = isSearching ? false : true
        navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: rightColor], for: .normal)
        
        navigationItem.leftBarButtonItem?.isEnabled = isSearching ? true : false
        navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: leftColor], for: .normal)
    }
    
    
    func setupCallBack(){
        guard let viewModel = self.viewModel else { return }
        
        viewModel.isSuccess.asObservable()
            .bind{ value in
                if(value) {
                    self.tableView.reloadData()
                }
            }.disposed(by: disposeBag)
        
        viewModel.errorMsg.asObservable()
            .bind { errorMessage in
                if(!errorMessage.isEmpty) {
                    self.alert(title: "Erro", error: errorMessage, buttonTexts: ["OK"])
                    print(errorMessage)
                }
            }.disposed(by: disposeBag)

    }

}

extension HomeView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = viewModel else { return }
        viewModel.selectedItem(by: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else { return 0 }
        return viewModel.rows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeTableViewCell
        
        guard let viewModel = viewModel, let enterprise = viewModel.getCompanyBy(indexPath.row) else {
            cell.nameLabel.text = "No company information found"
            return cell
        }
        
        cell.configureCell(model: enterprise)
        
        return cell
    }
    
    
}

extension HomeView: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let viewModel = self.viewModel else { return }
        viewModel.updateList(with: searchText)
        tableView.reloadData()
    }
    

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchBar.setShowsCancelButton(false, animated: true)
    }
}
