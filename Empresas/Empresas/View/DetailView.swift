//
//  DetailView.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 10/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import UIKit
import MaterialTextField
import RxCocoa
import RxSwift

class DetailView: UIViewController, Storyboarded {
    static var storyboard: StoryboardType = .detail
    var viewModel: DetailViewModel?
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        
        guard let viewModel = viewModel else { return }
        navigationItem.title = viewModel.enterpriseName
        descriptionLabel.text = viewModel.enterpriseDescription
    }
    
}
