//
//  HomeTableViewCell.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 09/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

        @IBOutlet weak var posterImage: UIImageView!
        @IBOutlet weak var nameLabel: UILabel!
        @IBOutlet weak var categoryLabel: UILabel!
        @IBOutlet weak var locationLabel: UILabel!
        
        override func awakeFromNib() {
            super.awakeFromNib()
//            selectionStyle = .none
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }
        
        func configureCell(model: Enterprise) {
            let logo = UIImage(named: "imgE1Lista.png")
            
            posterImage.image = logo
            
            if let companyName = model.enterprise_name {
                nameLabel.text = companyName
            }
            
            if let companyType = model.enterprise_type, let categoryName = companyType.enterprise_type_name {
                categoryLabel.text = "\(categoryName)"
            }
            
            if let companyLocation = model.country {
                locationLabel.text = companyLocation
            }
//
//            genreLabel.text = model.getFirstGenreId()
            
        }
    

    }
