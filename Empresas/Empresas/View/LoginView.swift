//
//  LoginView.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 18/12/19.
//  Copyright © 2019 Luiz Henrique. All rights reserved.
//

import UIKit
import MaterialTextField
import RxCocoa
import RxSwift

class LoginView: UIViewController, Storyboarded {
    static var storyboard: StoryboardType = .login
    
    
    @IBOutlet var emailTextField: MFTextField!
    @IBOutlet var passwordTextField: MFTextField!
    @IBOutlet var loginButton: UIButton!
    
    var viewModel : LoginViewModel?
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        setupBind()
        configureDismissKeyboard()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupCallBack()
    }
    
    private func setupBind() {
        guard let viewModel = self.viewModel else { return }
        
        emailTextField.rx.text.orEmpty
        .bind(to: viewModel.email)
        .disposed(by: disposeBag)
            
        passwordTextField.rx.text.orEmpty
        .bind(to: viewModel.password)
        .disposed(by: disposeBag)
        

        viewModel.isButtonEnabled
            .bind(to: loginButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        viewModel.isLoading.asObservable()
            .map({!$0})
            .bind(to: emailTextField.rx.isEnabled)
            .disposed(by: disposeBag)
        
        viewModel.isLoading.asObservable()
            .map({!$0})
            .bind(to: passwordTextField.rx.isEnabled)
            .disposed(by: disposeBag)
        
        loginButton.rx.tap.do(onNext:  { [weak self] in
            guard let `self` = self else { return }
            self.emailTextField.resignFirstResponder()
            self.passwordTextField.resignFirstResponder()
        }).subscribe({ _ in
            viewModel.performLogin()
        }).disposed(by: disposeBag)
    }
    
    func setupCallBack(){
        guard let viewModel = self.viewModel else { return }
        
        viewModel.isSuccess.asObservable()
            .bind{ value in
                if(value) {
                    print("Login successfully")
                }
            }.disposed(by: disposeBag)
        
        viewModel.errorMsg.asObservable()
            .bind { errorMessage in
                if(!errorMessage.isEmpty) {
                    self.alert(title: "Erro", error: errorMessage, buttonTexts: ["OK"])
                    print(errorMessage)
                }
            }.disposed(by: disposeBag)

    }
    
    
    
}
