//
//  BaseNavigationController.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 09/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import Foundation
import UIKit

class BaseNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.isTranslucent = false
        

        self.navigationBar.tintColor = .white
        self.navigationBar.barTintColor = UIColor(displayP3Red: 222/255.0, green: 71/255.0, blue: 114/255.0, alpha: 1)
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        


        
//       addNavBarImage()
        //addNavBarTitle()
    }
    func addNavBarTitle() {
        navigationItem.title = "ioasys"
        
    }
    
    func addNavBarImage() {

        let logo = UIImage(named: "logoNav.png")

        let imageView = UIImageView(image:logo)
        imageView.contentMode = .top
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        navigationBar.addSubview(imageView)

        navigationBar.addConstraint (navigationBar.leftAnchor.constraint(equalTo: imageView.leftAnchor, constant: 0))
        navigationBar.addConstraint (navigationBar.rightAnchor.constraint(equalTo: imageView.rightAnchor, constant: 0))
        navigationBar.addConstraint (navigationBar.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 0))
        navigationBar.addConstraint (navigationBar.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 0))
    }
}
