//
//  EnterpriseType.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 20/12/19.
//  Copyright © 2019 Luiz Henrique. All rights reserved.
//

import Foundation

public class EnterpriseType: Codable {
    var id: Int?
    var enterprise_type_name: String?
}
