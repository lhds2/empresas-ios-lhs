//
//  Enterprise.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 20/12/19.
//  Copyright © 2019 Luiz Henrique. All rights reserved.
//

import Foundation

public class Enterprise: Codable {
    var id: Int?
    var email_enterprise: String?
    var facebook: String?
    var twitter: String?
    var linkedin: String?
    var phone: String?
    var own_enterprise: Bool?
    var enterprise_name: String?
    var photo: String?
    var description: String?
    var city: String?
    var country: String?
    var value: Int?
    var share_price: Double?
    var enterprise_type: EnterpriseType?

}
