//
//  Investidor.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 20/12/19.
//  Copyright © 2019 Luiz Henrique. All rights reserved.
//

import Foundation

public class Investor: Codable {
    var id: Int?
    var investor_name: String?
    var email: String?
    var city: String?
    var country: String?
    var balance: Double?
    var photo: String?
    var portfolio: Portfolio?
    var portfolio_value: Double?
    var first_access: Bool?
    var super_angel: Bool?
    
}
