//
//  Companies.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 10/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import Foundation

struct Companies: Codable {
    var enterprises_number: Int = 0
    var enterprises: [Enterprise]?
    
    public init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)
        enterprises = try container.decodeArray(Enterprise.self, forKey: .enterprises)
        enterprises_number = enterprises?.count ?? 0
    }
}

