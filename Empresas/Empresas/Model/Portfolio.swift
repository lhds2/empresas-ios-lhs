//
//  Portfolio.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 20/12/19.
//  Copyright © 2019 Luiz Henrique. All rights reserved.
//

import Foundation

struct Portfolio: Codable {
    var enterprises_number: Int?
    var enterprises: [Enterprise]?
    
    public init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)
        enterprises_number = try! container.decode(Int.self, forKey: .enterprises_number)
        enterprises = try container.decodeArray(Enterprise.self, forKey: .enterprises)
    }
}

