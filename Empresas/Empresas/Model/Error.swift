//
//  Error.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 18/12/19.
//  Copyright © 2019 Luiz Henrique. All rights reserved.
//

import Foundation

public struct CustomError : Error {
    public enum CustomErrorType {
        case wrongEmailValidation
        case wrongPasswordValidation
        case serviceError
        case unknownError
        case connectionError
        case invalidRequest
        case invalidCredentials
        case notFound
        case parserError
    }
    
    init(_ error: Error, type: CustomErrorType = .notFound) {
        self.localizedDescription = error.localizedDescription
        self.errorType = type
    }
    
    init(with type: CustomErrorType = .notFound) {
        self.localizedDescription = ""
        self.errorType = type
    }
    //Static versions
    
    var localizedDescription: String
    var errorType: CustomErrorType = .notFound
    
    func description()->String {
        switch self.errorType {
        case .wrongEmailValidation:
            return "O E-mail digitado não é valido. Favor inserir credenciais corretas."
        case .wrongPasswordValidation:
            return "A senha digitada não é valida. Favor inserir uma senha valida."
        case .serviceError:
            return self.localizedDescription
        case .unknownError:
            return "Um erro foi encontrado durante obtenção dos dados. Tente novamente mais tarde"
        case .connectionError:
            return "Aparentemente sua internet está bem lenta. Tente novamente quando sua conexão melhorar"
        case .invalidRequest:
            return "Erro na API."
        case .invalidCredentials:
            return "Credenciais invalidas. Favor tentar novamente com as credenciais corretas"
        case .notFound:
            return "A API não pode obter mais informações sobre essa empresa."
        case .parserError:
            return "Existe um problema na obtenção das informações."
        }
    }
}

