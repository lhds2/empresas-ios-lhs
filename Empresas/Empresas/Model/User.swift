//
//  User.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 19/12/19.
//  Copyright © 2019 Luiz Henrique. All rights reserved.
//

import Foundation

public class User: Codable {
    var investor : Investor?
    var enterprise : Enterprise?
    var success    : Bool?
}

public class Session: Codable {
    var token: String
    var client: String
    var uid: String
    
    init(Token: String, Client: String, Uid: String) {
        token = Token
        client = Client
        uid = Uid
    }
}
