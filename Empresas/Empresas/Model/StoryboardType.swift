//
//  StoryboardType.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 08/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import Foundation

enum StoryboardType: String {
    case login = "Login"
    case home = "Home"
    case detail = "Detail"
}
