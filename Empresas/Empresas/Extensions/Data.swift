//
//  Data.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 08/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import Foundation

struct Empty: Codable { }

extension Data {
    func asObject<T:Codable>(_ type: T.Type) -> T? {
        if type == Empty.self {
            return Empty() as? T
        }
        return try? JSONDecoder().decode(type, from: self)
    }
    
}
