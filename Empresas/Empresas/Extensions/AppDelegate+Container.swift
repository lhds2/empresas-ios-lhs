//
//  AppDelegate.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 08/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import Foundation
import Swinject
import SwinjectAutoregistration

extension AppDelegate {
    internal func setupContainer() {
        setupCoordinators()
        setupServices()
        setupViewModels() 
    }
    
    private func setupCoordinators(){
        AppDelegate.container.autoregister(AppCoordinator.self, initializer: AppCoordinator.init)
        AppDelegate.container.autoregister(LoginCoordinator.self, initializer:  LoginCoordinator.init)
        AppDelegate.container.autoregister(HomeCoordinator.self, initializer:  HomeCoordinator.init)
        AppDelegate.container.autoregister(DetailCoordinator.self, initializer:  DetailCoordinator.init)

        
    }

    private func setupServices(){
        AppDelegate.container.autoregister(UserDataManager.self, initializer: UserDataManager.init).inObjectScope(.container)
    }
    private func setupViewModels(){
        AppDelegate.container.autoregister(LoginViewModel.self, initializer: LoginViewModel.init)
        AppDelegate.container.autoregister(HomeViewModel.self, initializer: HomeViewModel.init)
        AppDelegate.container.autoregister(DetailViewModel.self, initializer: DetailViewModel.init)
    }

}
