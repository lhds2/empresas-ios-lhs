//
//  DataManager.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 08/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import Foundation

protocol DataManager {
    
    func get<T>(key: String, type: T.Type) -> T? where T : Codable

    func get(key: String) -> String?
    
    func set<T>(key: String, value: T?) where T : Codable
    
    func remove(key: String)
    
    func clear()
}
