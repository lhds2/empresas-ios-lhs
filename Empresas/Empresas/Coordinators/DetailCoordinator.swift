//
//  DetailCoordinator.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 10/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import Foundation

class DetailCoordinator: BaseCoordinator {
    private let viewModel: DetailViewModel
    
    init(viewModel: DetailViewModel) {
        self.viewModel = viewModel
    }
    
    override func start() {
        let viewController = DetailView.instantiate()
        viewController.viewModel = viewModel
        
        self.navigationController.isNavigationBarHidden = true
        self.navigationController.viewControllers = [viewController]
    }
}
