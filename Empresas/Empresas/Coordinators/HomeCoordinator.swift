//
//  HomeCoordinator.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 09/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class HomeCoordinator: BaseCoordinator {
    
    private let viewModel: HomeViewModel
    private let detailViewModel: DetailViewModel
    private let disposebag = DisposeBag()
    
    init(viewModel: HomeViewModel, detailViewModel: DetailViewModel ) {
        self.viewModel = viewModel
        self.detailViewModel = detailViewModel
    }
    
    override func start() {
        self.setUpBindings()
        
        let viewController = HomeView.instantiate()
        viewController.viewModel = viewModel
        
        self.navigationController.isNavigationBarHidden = true
        self.navigationController.viewControllers = [viewController]
    }
    
    
    private func didSelectEnterprise() {
        let viewController = DetailView.instantiate()
        viewController.viewModel = self.detailViewModel
        viewController.viewModel?.enterpriseInformation = viewModel.selectedInformation
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
    private func setUpBindings() {
        self.viewModel.didSelectedRow
            .subscribe(onNext: { [weak self] in self?.didSelectEnterprise() })
            .disposed(by: self.disposebag)
        
    }
}
