//
//  AppCoordinator.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 08/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import Foundation
import RxSwift

class AppCoordinator: BaseCoordinator {

    private let disposeBag = DisposeBag()
    private let user = UserDataManager()
    private var window = UIWindow(frame: UIScreen.main.bounds)
    
    
    override func start() {
        self.window.makeKeyAndVisible()
        
        self.user.isLogged
            ? self.showHome()
            : self.showLogin()
    }
    
    private func showHome() {
        self.removeChildCoordinators()
        
        let coordinator = AppDelegate.container.resolve(HomeCoordinator.self)!
        coordinator.navigationController = BaseNavigationController()
        self.start(coordinator: coordinator)
        
        ViewControllerUtils.setRootViewController(
            window: self.window,
            viewController: coordinator.navigationController,
            withAnimation: true)
    }
    
    private func subscribeToLogin() {
        // TODO: Fix navigation
        self.user.didSignIn
            .subscribe(onNext: {
                [weak self] in self?.showHome()
            })
        .disposed(by: self.disposeBag)
        //
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .NSURLCredentialStorageChanged, object: nil)
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        NotificationCenter.default.removeObserver(self, name: .NSURLCredentialStorageChanged, object: nil)
        self.showHome()
    }
    
   private func showLogin() {
       self.removeChildCoordinators()
       self.subscribeToLogin()
       
       let coordinator = AppDelegate.container.resolve(LoginCoordinator.self)!
       self.start(coordinator: coordinator)
       
       ViewControllerUtils.setRootViewController(
           window: self.window,
           viewController: coordinator.navigationController,
           withAnimation: true)
   }

}
