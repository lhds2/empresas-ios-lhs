//
//  LoginCoordinator.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 08/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import Foundation
import UIKit

class LoginCoordinator: BaseCoordinator {
    
    private let viewModel: LoginViewModel
    
    init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
    }
    
    override func start() {
        let viewController = LoginView.instantiate()
        viewController.viewModel = viewModel
        
        self.navigationController.isNavigationBarHidden = true
        self.navigationController.viewControllers = [viewController]
    }
}
