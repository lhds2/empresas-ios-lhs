//
//  Service.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 19/12/19.
//  Copyright © 2019 Luiz Henrique. All rights reserved.
//

import Foundation
import Alamofire

class Service {
    
    enum Result<Value, Error: Swift.Error> {
        case success(Data)
        case failure(Error)
    }
    
    typealias Handler = (Result<Data, Error>) -> Void

    
    static let shared: Service = Service()
    
    private init() {
        manager = Alamofire.SessionManager()
    }
    
    static let BASE_URL = "https://empresas.ioasys.com.br/api/v1/"
    private let manager: Alamofire.SessionManager!
    

    enum Endpoint: String {
        case signIn = "users/auth/sign_in"
        case enterpriseIndex = "enterprises?enterprise_types=%@&name=%@"
        case showEnterpriseList = "enterprises"
        case showEnterpriseInfo = "enterprises/%@"
        
        internal var method: HTTPMethod {
            switch self {
            case .signIn             : return .post
            case .showEnterpriseList : return .get
            case .enterpriseIndex    : return .get
            case .showEnterpriseInfo : return .get
            }
        }
        
        fileprivate var headers: HTTPHeaders {
            switch self {
            case .signIn:
                return [
                    "Authorization": "",
                    "Accept": "application/json"
                ]
            default:
                return requestCurrentHeader()
            }
        }
        
        private func requestCurrentHeader() -> HTTPHeaders {
            guard let session = UserDefaults.standard.data(forKey: "session")?.asObject(Session.self) else { return [:] }
            let header : HTTPHeaders = ["client": session.client,
                                        "access-token": session.token ,
                                        "uid": session.uid,
                                        "Content-Type": "application/json"]
            return header
        }
        
    }
    
        func request(_ endpoint        : Endpoint,
                     _ pathParamenters : [CVarArg]?,
                     _ parameters      : [String:Any]?,
                     handler: @escaping Handler) {
        
        
        var requestURL = Service.BASE_URL + endpoint.rawValue
        
        if pathParamenters != nil && (pathParamenters?.count)! > 0 {
            requestURL = String.init(format: requestURL, arguments: pathParamenters!)
        }
        
        let encoding: ParameterEncoding = JSONEncoding.default
            manager.request(requestURL, method: endpoint.method, parameters: parameters, encoding: encoding, headers: endpoint.headers)
                //.validate(statusCode: 200..<299)
                .responseJSON(completionHandler: { response in
                    if (requestURL.contains(Endpoint.signIn.rawValue)), let header = response.response?.allHeaderFields {
                        self.store(header)
                    }
                    switch response.result {
                    case .success(_):
                        if let data = response.data {
                           handler(.success(data))
                        }
                        
                    case .failure(let error):
                        handler(.failure(error))

                    }
                })
                
        }
    
    func store(_ allHeader: [AnyHashable : Any]) {
        guard let header = allHeader as NSDictionary? as! [String:String]? else {
            return
        }
        
        if let token : String = header.value(for: "access-token") as? String,
            let client = header.value(for: "client") as? String,
            let uid = header.value(for: "uid") as? String {
            let session =  Session(Token: token, Client: client, Uid: uid)
            UserDefaults.standard.set(session.toJson(), forKey: "session")
            
            
        }
        
    }

    
}
