//
//  LoginService.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 19/12/19.
//  Copyright © 2019 Luiz Henrique. All rights reserved.
//

import Foundation
import Alamofire

class LoginService {
    
    public func loginPostWith(user: String,
                              password: String,
                              completion: @escaping (User?, CustomError?) -> Void) {
        let body = ["email": user, "password": password]
        Service.shared.request(.signIn,
                               nil,
                               body) { result in
                                switch result {
                                case .failure( let error):
                                    let customError = CustomError(error, type: .serviceError)
                                    
                                    completion(nil, customError)
                                case .success(let data):
                                    do {
                                        let user = try JSONDecoder().decode(User.self, from: data)
                                        completion(user, nil)
                                    } catch {
                                        completion(nil, CustomError(with:.parserError))
                                    }
                                    
                                }
                                
        }
                                
    }
    
    private func getBodyParameters(_ user: String,_ password: String){
        
    }
    
}
