//
//  HomeService.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 10/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import Foundation
import Alamofire

class HomeService {
    
    public func requestCompanyList(
                              completion: @escaping (Companies?, CustomError?) -> Void) {
        Service.shared.request(.showEnterpriseList,
                               nil,
                               nil) { result in
                                switch result {
                                case .failure( let error):
                                    let customError = CustomError(error, type: .serviceError)
                                    
                                    completion(nil, customError)
                                case .success(let data):
                                    do {
                                        let companyList = try JSONDecoder().decode(Companies.self, from: data)
                                        completion(companyList, nil)
                                    } catch {
                                        completion(nil, CustomError(with:.parserError))
                                    }
                                    
                                }
                                
        }
                                
    }
    
    
}
