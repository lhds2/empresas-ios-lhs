//
//  UserDataManager.swift
//  Empresas
//
//  Created by Luiz Henrique de Sousa on 08/01/20.
//  Copyright © 2020 Luiz Henrique. All rights reserved.
//

import Foundation
import RxSwift

class UserDataManager: DataManager {
    private var _user: User?
    private var _session: Session?
    private let signInSubject = PublishSubject<Void>()
    private let disposeBag = DisposeBag()
    
    var isLogged : Bool {
        guard let session = _session else { return false }
        return !session.token.isEmpty
        
    }
    
    var token : String {
        return _session?.token ?? ""
    }
    
    var didSignIn: Observable<Void> {
        return self.signInSubject.asObservable()
    }
    
    init() {
        self._user = UserDefaults.standard.data(forKey: "user")?.asObject(User.self) ?? nil
        self._session = UserDefaults.standard.data(forKey: "session")?.asObject(Session.self) ?? nil
        
    }
    
    func get<T>(key: String, type: T.Type) -> T? where T : Codable {
        if let user = self._user, user.hasKey(for: key){
            return user.value(for: key) as? T
        }
        return nil
    }
    
    internal func get(key: String) -> String? {
        fatalError("Not applicable")
    }
    
    func register(session: Session) {
        UserDefaults.standard.set(session.toJson(), forKey: "session")
        self._session = session
    }
    
    @discardableResult
    func reloadSession()->Bool {
        if let session = UserDefaults.standard.data(forKey: "session")?.asObject(Session.self) {
            _session = session
            //self.signInSubject.onNext(Void())
            NotificationCenter.default.post(name: .NSURLCredentialStorageChanged, object: nil)
            return true
        }
        return false
    }
    
    func set<T>(key: String, value: T?) where T : Codable {
        if let _ = value,
           let user = _user,
            user.hasKey(for: key){
            self._user = user.setValue(for: key, with: "attribute")
        }
        
    }
    
    func remove(key: String) {
        self.synchronize()
    }
    
    func clear() {
        self._user = nil
        self.synchronize()
    }
    
    private func synchronize() {
        UserDefaults.standard.set(self._user.toJson(), forKey: "data")
    }
}
